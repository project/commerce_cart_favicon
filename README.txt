Drupal Commerce Cart Favicon

This module places a badge on site's favicon with the total items of shopping cart.

Installation
This module requires Commerce Cart (included on Drupal Commerce).
Commerce Cart Favicon uses Favico.js plugin (http://lab.ejci.net/favico.js) and it's already included on the module files for an easier installation.
Just download and enable the module.

Settings
To customize the module settings go to [site_url]/admin/commerce/config/cart-favicon

More information about favico.js plugin: http://lab.ejci.net/favico.js

Author of this module: Marco Fernandes (marcofernandes75@gmail.com)